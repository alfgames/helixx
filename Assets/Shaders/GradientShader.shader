﻿Shader "Alf/Gradient"
{
    Properties
    {
        _StartColor("Start Color", Color) = (1, 0, 0, 1)
        _EndColor("End Color", Color) = (0, 1, 0, 1)
        _Offset("Offset", Range(0, 100)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            float4 _StartColor;
            float4 _EndColor;
            float _Offset;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float screen = 10;
                float startOffset = (_Offset - (screen / 2)) / 100;
                float endOffset = (_Offset + (screen / 2)) / 100;
                float4 startColor = lerp(_StartColor, _EndColor, startOffset);
                float4 endColor = lerp(_StartColor, _EndColor, endOffset);
                float4 color = lerp(startColor, endColor, i.uv.y);
                return color;
            }
            ENDCG
        }
    }
}
