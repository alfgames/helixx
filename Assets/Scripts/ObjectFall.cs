﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFall : MonoBehaviour
{

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Player")
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.isKinematic = false;
            //rb.AddExplosionForce(3, FindObjectOfType<HelixController>().player.transform.position, 4, 6, ForceMode.Impulse);
            
            Player player = other.gameObject.GetComponent<Player>();
            if (player != null)
            {
                player.OnObjectFall(gameObject);                
            }
            Destroy(this.gameObject, 2);
        }
    }
}
