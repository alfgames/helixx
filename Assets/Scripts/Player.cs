﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer
{
    void PlayerDidFallObject(Player player, GameObject obj);
    void PlayerDidHitObstacle(Player player, GameObject obj);
    void PlayerDidHitCompletion(Player player, GameObject obj);
}

public class Player : MonoBehaviour
{

    public IPlayer playerDelegate;

    
    public void OnObjectFall(GameObject obj)
    {
        if (playerDelegate != null)
        {
            playerDelegate.PlayerDidFallObject(this, obj);
        }
    }

    public void OnObstacleHit(GameObject obj)
    {
        if (playerDelegate != null)
        {
            playerDelegate.PlayerDidHitObstacle(this, obj);
        }
    }

    public void OnCompletionHit(GameObject obj)
    {
        if (playerDelegate != null)
        {
            playerDelegate.PlayerDidHitCompletion(this, obj);
        }
    }

}
