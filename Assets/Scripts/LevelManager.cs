﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using MoreMountains.NiceVibrations;

public class LevelManager : MonoBehaviour, ILevel, IMenuManager
{
    
    private static string CurrentLevelIndexKey = "CurrentLevelIndex";
    private static string DisplayingLevelKey = "DisplayingLevel";
    
    public int levelCount;
    public int forceLevel;
    
    public HelixController helixController;
    public MenuManager menuManager;
    
    private int currentLevelIdx;
    private int displayingLevelIdx;

    private void Start()
    {
        Application.targetFrameRate = 60;
        GameAnalytics.Initialize();

        menuManager.menuDelegate = this;
        
        displayingLevelIdx = PlayerPrefs.GetInt(DisplayingLevelKey, 0);
        int lastPlayedLevelIdx = PlayerPrefs.GetInt(CurrentLevelIndexKey, 0);
            
        if (forceLevel >= 0)
        {
            lastPlayedLevelIdx = forceLevel;
        }
            
        LoadLevel(lastPlayedLevelIdx);
    }
    
    private void LoadLevel(int levelIdx)
    {
        if (levelIdx >= levelCount)
        {
            levelIdx = Random.Range(0, levelCount);
        }
            
        var currentLevelObj = Instantiate(Resources.Load("Levels/" + "Level" + levelIdx) as GameObject);
        var level = currentLevelObj.GetComponentInChildren<Level>();
        level.levelDelegate = this;
        helixController.player = level.player;
        
        
        Camera.main.GetComponent<CameraFollow>().target = level.player.transform;
        
        currentLevelIdx = levelIdx;
            
        //TODO: Score, etc
            
        PlayerPrefs.SetInt(CurrentLevelIndexKey, levelIdx);
        PlayerPrefs.Save();
            
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level" + currentLevelIdx);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "DisplayLevel" + displayingLevelIdx);
    }

    public void LevelDidComplete(Level level)
    {
        helixController.InputUp();
        menuManager.OpenLevelSuccesPanel();
        MMVibrationManager.Haptic(HapticTypes.Success);
    }

    public void LevelDidFail(Level level)
    {
        menuManager.OpenFailedLevelPanel();
        MMVibrationManager.Haptic(HapticTypes.Failure);
    }

    public void MenuManagerNextLevelTapped(MenuManager menuManager)
    {
        displayingLevelIdx = PlayerPrefs.GetInt(DisplayingLevelKey, 0);
        displayingLevelIdx++;
        PlayerPrefs.SetInt(DisplayingLevelKey, displayingLevelIdx);
        
        int lastPlayedLevelIdx = PlayerPrefs.GetInt(CurrentLevelIndexKey, 0);
        lastPlayedLevelIdx++;
        PlayerPrefs.SetInt(CurrentLevelIndexKey, lastPlayedLevelIdx);
        
        PlayerPrefs.Save();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void MenuManagerRestartLevelTapped(MenuManager menuManager)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
