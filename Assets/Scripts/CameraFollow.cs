﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothspeed;
    public Vector3 offset;
    public bool isPlaying = true;
    public float firstOffset;
    Vector3 supportvec;

    private void Start()
    {       
        firstOffset = offset.z;
    }

    void FixedUpdate()
    {
        if (isPlaying)
        {
            supportvec = new Vector3(target.position.x, 0f, target.position.z);

            Vector3 desiredPosition = (target.position - supportvec) + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothspeed * Time.deltaTime);
            transform.position = smoothedPosition;
        }
    }
}
