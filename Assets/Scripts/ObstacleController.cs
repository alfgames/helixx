﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
using MoreMountains.NiceVibrations;

public class ObstacleController : MonoBehaviour
{
    CameraFollow cameraFollow;
    HelixController helixController;

    [SerializeField]
    private GameObject mainParent;
    [SerializeField]
    private GameObject particle;

    private void Start()
    {
        cameraFollow = FindObjectOfType<CameraFollow>();
        helixController = FindObjectOfType<HelixController>();
    }

    private void ObstacleExplosion()
    {
        Animator anim = GetComponent<Animator>();
        if (anim == null)
        {
            anim = GetComponentInParent<Animator>();
        }
        CameraShaker.Instance.ShakeOnce(1.2f, 2f, .1f, .3f);
        GameObject explosionPart = Instantiate(particle, helixController.player.transform.position, Quaternion.identity);
        Destroy(explosionPart, 1.5f);
        anim.enabled = false;
        mainParent.transform.GetChild(1).gameObject.SetActive(true);
        mainParent.transform.GetChild(0).gameObject.SetActive(false);
        BoxCollider thisColl = GetComponent<BoxCollider>();
        thisColl.enabled = false;
        MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == "Player")
        {
            if (helixController.isPlay && !helixController.isFevering)
            {
                Animator anim = GetComponent<Animator>();
                if (anim == null)
                {
                    anim = GetComponentInParent<Animator>();
                }
                anim.enabled = false;
                helixController.isPlay = false;
                cameraFollow.target = other.transform;

                Player player = FindObjectOfType<Player>();
                if (player != null)
                {
                    player.OnObstacleHit(gameObject);                
                }
            }
            else if(helixController.isPlay && helixController.isFevering)
            {
                //ObstacleExplosion();
                //Animator anim = GetComponent<Animator>();
                //if (anim == null)
                //{
                //    anim = GetComponentInParent<Animator>();
                //}
                //CameraShaker.Instance.ShakeOnce(1.2f, 2f, .1f, .3f);
                //anim.enabled = false;
                //mainParent.transform.GetChild(1).gameObject.SetActive(true);
                //mainParent.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (this.gameObject.name == "TriggerColl")
            {
                if (helixController.isFevering)
                {
                    ObstacleExplosion();
                }
            }
        }
    }
}
