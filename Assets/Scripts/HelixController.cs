﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TrailsFX;

public class HelixController : MonoBehaviour
{
    CameraFollow cameraFollow;

    public GameObject player;
    public GameObject helix;
    public bool isPlay = true;

    public float needTimeForFever;
    public float executeTimeForFever;
    public bool isFevering = false;
    public float needTimeCounterFever = 0;
    public Image feverBar;
    public Image feverCountImage;
    public Image feverDecreaseImage;
    public GameObject feverParticle;
    public Image whiteBackground;
    public Text holdAndReleaseText;

    public List<GameObject> helixList = new List<GameObject>();

    bool isMoving;
    int currentCount = 1;
    int removePos = 1;
    float firstMovementSpeed, firstBackMovementSpeed;
    int createCount, destroyCount;

    [SerializeField]
    private float movementSpeed, backMovementSpeed;
    [SerializeField]
    private GameObject helixParent;
    [SerializeField]
    private float feverCamSpeed;

    private void Start()
    {
        firstBackMovementSpeed = backMovementSpeed;
        firstMovementSpeed = movementSpeed;
        cameraFollow = FindObjectOfType<CameraFollow>();
    }

    void Update()
    {
        if (isPlay)
        {
            if ((int)player.transform.position.y == currentCount)
            {
                currentCount++;
                CreateHelix();
            }
            if (isFevering)
            {
                //if (Camera.main.fieldOfView <= 65)
                //{
                //    Camera.main.fieldOfView += 50 * Time.deltaTime;
                //}
                if (cameraFollow.offset.z >= -40)
                {
                    cameraFollow.offset.z -= feverCamSpeed * Time.deltaTime;
                }
            }
            if (isMoving)
            {
                if (!isFevering)
                {
                    needTimeCounterFever += Time.deltaTime;
                    UpdateFeverImage((needTimeCounterFever / needTimeForFever));
                    if (needTimeCounterFever >= needTimeForFever)
                    {
                        isFevering = true;
                        whiteBackground.color = new Color(0,1,0,0.28f);
                        Instantiate(feverParticle, player.transform.position, Quaternion.identity);
                        TrailEffect trail = player.GetComponent<TrailEffect>();
                        trail.colorSequence = ColorSequence.PingPong;
                    }
                }
                player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + movementSpeed, player.transform.position.z);
                player.transform.Rotate(0, -360.0f / (1 / movementSpeed), 0);
                if (cameraFollow.offset.z >= -33)
                {
                    cameraFollow.offset.z -= 3 * Time.deltaTime;
                }
            }
            else if (removePos < helixList.Count && helixList[removePos] != null)
            {
                if ((int)helixList[removePos - 1].transform.position.y == removePos)
                {
                    removePos++;
                    RemoveHelix();
                }
                else if ((int)helixList[removePos - 1].transform.position.y > removePos)
                {
                    removePos++;
                    RemoveHelix();
                }

                helixList[removePos - 1].transform.position = new Vector3(helixList[removePos - 1].transform.position.x, helixList[removePos - 1].transform.position.y + backMovementSpeed, helixList[removePos - 1].transform.position.z);
                helixList[removePos - 1].transform.Rotate(0, -360.0f / (1 / backMovementSpeed), 0);
                if (cameraFollow.offset.z <= cameraFollow.firstOffset)
                {
                    cameraFollow.offset.z += 12 * Time.deltaTime;
                }
            }
            else
            {
                if (cameraFollow.offset.z <= cameraFollow.firstOffset)
                {
                    cameraFollow.offset.z += 12 * Time.deltaTime;
                }
            }
        }
        else
        {
            if (cameraFollow.offset.z <= cameraFollow.firstOffset)
            {
                cameraFollow.offset.z += 12 * Time.deltaTime;
            }
     
        }

   

        if (!isMoving)
        {
            if (destroyCount + 8 >= createCount)
            {
                backMovementSpeed = 1.25f;
            }
            else
            {
                backMovementSpeed = 8;
            }
            if (!isFevering)
            {
                if (needTimeCounterFever >= 0)
                {
                    needTimeCounterFever -= Time.deltaTime;
                    UpdateFeverImage(needTimeCounterFever / needTimeForFever);
                }
            }
           
        }

        if (isFevering)
        {
            executeTimeForFever -= Time.deltaTime;
            UpdateFeverImage((executeTimeForFever / needTimeForFever));
            if (executeTimeForFever <= 0f)
            {
                executeTimeForFever = 3f;
                needTimeCounterFever = 0f;
                isFevering = false;
                whiteBackground.color = new Color(1,1,1,0.28f);
                TrailEffect trail = player.GetComponent<TrailEffect>();
                trail.colorSequence = ColorSequence.Fixed;
            }
            movementSpeed = firstMovementSpeed * 1.8f;
            //backMovementSpeed = firstBackMovementSpeed+.5f;

        }else
        {
            movementSpeed = firstMovementSpeed;
            //backMovementSpeed = firstBackMovementSpeed;
            //if (Camera.main.fieldOfView >= 60)
            //{
            //    Camera.main.fieldOfView -= 40 * Time.deltaTime;
            //}
        }

    }

    public void InputDown()
    {
        isMoving = true;
        holdAndReleaseText.gameObject.SetActive(false);
    }
    public void InputUp()
    {
        isMoving = false;
    }

    private void CreateHelix()
    {
        GameObject dummyHelix = Instantiate(helix, new Vector3(player.transform.position.x, currentCount - 1, player.transform.position.z), Quaternion.identity);
        dummyHelix.transform.parent = helixParent.transform;
        helixList.Add(dummyHelix);
        createCount++;
    }
    private void RemoveHelix()
    {
        Destroy(helixList[removePos - 2].gameObject);
        destroyCount++;
    }

    private void UpdateFeverImage(float progress)
    {
        feverBar.fillAmount = progress;  
        // if (isFevering)
        // {
        //     feverCountImage.gameObject.SetActive(false);
        //     feverDecreaseImage.gameObject.SetActive(true);
        //     feverDecreaseImage.fillAmount = progress;
        // }
        // else
        // {
        //     feverDecreaseImage.gameObject.SetActive(false);
        //     feverCountImage.gameObject.SetActive(true);
        //     feverCountImage.fillAmount = progress;            
        // }
    }
}
