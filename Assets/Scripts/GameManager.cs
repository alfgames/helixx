﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
   public void RestartScene()
   {
      SceneManager.LoadScene(SceneManager.GetActiveScene().name);
   }
}
