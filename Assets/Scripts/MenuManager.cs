﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IMenuManager
{
    void MenuManagerNextLevelTapped(MenuManager menuManager);
    void MenuManagerRestartLevelTapped(MenuManager menuManager);
}

public class MenuManager : MonoBehaviour
{
    public CanvasRenderer passedPanel;
    public CanvasRenderer failedPanel;
    public CanvasRenderer gamePanel;

    public AudioSource succesLevelSound;
    public GameObject particleObject;

    public Text levelText, nextLevelText;
    public Image levelFillImage;
    private Level level;

    public IMenuManager menuDelegate;

    void Start()
    {
        SetLevelText();  
    }

    void Update()
    {
        levelFillImage.fillAmount = FindObjectOfType<Level>().GetCurrentProgress();
    }

    public void NextLevelButtonTapped()
    {
        if (menuDelegate != null)
        {
            menuDelegate.MenuManagerNextLevelTapped(this);
        }
    }

    public void RestartLevelButtonTapped()
    {
        if (menuDelegate != null)
        {
            menuDelegate.MenuManagerRestartLevelTapped(this);
        }
    }

    public void OpenLevelSuccesPanel()
    {
        succesLevelSound.Play();
        particleObject.gameObject.SetActive(true);
        passedPanel.gameObject.SetActive(true);
        gameObject.gameObject.SetActive(false);

    }

    public void OpenFailedLevelPanel()
    {
        failedPanel.gameObject.SetActive(true);
        gameObject.gameObject.SetActive(false);
    }

    public void SetLevelText()
    {
        levelText.text = PlayerPrefs.GetInt("DisplayingLevel", 1) + "";
        nextLevelText.text = PlayerPrefs.GetInt("DisplayingLevel", 1) + 1 + "";
    }




}
