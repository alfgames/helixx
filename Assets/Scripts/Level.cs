﻿using System;
using UnityEngine;
using MoreMountains.NiceVibrations;

public interface ILevel
{
    void LevelDidComplete(Level level);
    void LevelDidFail(Level level);
}

public class Level : MonoBehaviour, IPlayer
{

    public GameObject finalObj;
    public GameObject player;
    public Material backgroundMat;
    public Material helixCloneMat;
    public Material characterMat;

    public ILevel levelDelegate;
    
    private Vector3 startPosition;

    private void Start()
    {
        player.GetComponent<Player>().playerDelegate = this;
        helixCloneMat.color = characterMat.color;
    }

    private void Update()
    {
        UpdateBackgroundColor();
    }

    private void UpdateBackgroundColor()
    {
        float value = (player.transform.position.y - startPosition.y) / (finalObj.transform.position.y - startPosition.y);
        backgroundMat.SetFloat("_Offset", value * 100);
    }

    public void PlayerDidFallObject(Player player, GameObject obj)
    {
        MMVibrationManager.Haptic(HapticTypes.LightImpact);
    }

    public void PlayerDidHitObstacle(Player player, GameObject obj)
    {
        if (levelDelegate != null)
        {
            levelDelegate.LevelDidFail(this);
        }
    }

    public void PlayerDidHitCompletion(Player player, GameObject obj)
    {
        if (levelDelegate != null)
        {
            levelDelegate.LevelDidComplete(this);
        }
    }

    public float GetCurrentProgress()
    {
        return  Vector3.Distance(startPosition, player.transform.position) / Vector3.Distance(startPosition, finalObj.transform.position);
    }


}
